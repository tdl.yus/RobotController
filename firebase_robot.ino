#include <FirebaseArduino.h>
#include <ESP8266WiFi.h>

#define FIREBASE_HOST "robotcontrol14.firebaseio.com"
#define FIREBASE_AUTH "RASVEEafCt6xr4pEE6xkEf6hedPjTfqnFzhMAG7H"
#define WIFI_SSID "Samudera"
#define WIFI_PASSWORD "wifinembahmu"
#define asap A0

int id = 0;
int num = 0;
char buffer[15];
float ppm = 8;
StaticJsonBuffer<150> jsonBuffer;
JsonObject& timestamp = jsonBuffer.createObject();
JsonObject& updateData = jsonBuffer.createObject();

int maju, mundur, kanan, kiri, berhenti;

//variable parsing data
String dataIn;
String dt[10];
int i;
boolean parsing = false;

void setup() {
  Serial.begin(9600);
  // connect to wifi.
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("connecting");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.print("connected: ");
  Serial.println(WiFi.localIP());

  timestamp[".sv"] = "timestamp";
  Serial.println();
  timestamp.prettyPrintTo(Serial);
  Firebase.begin(FIREBASE_HOST);
  FirebaseObject object = Firebase.get("/");
  id = object.getInt("id");
  //  Serial.print("id = ");
  //  Serial.println(id);
  if (id == 0) {
    num = 0;
  } else {
    num = id + 1;
  }
}

void loop() {
  gas();
  getData();
  updatePpm(/*data sensor*/ ppm);
}

void getData() {
  FirebaseObject object = Firebase.get("direction/");
  if (Firebase.failed()) {
    Serial.println("Firebase get failed");
    Serial.println(Firebase.error());
    return;
  } else {
    kanan = object.getInt("kanan");
    kiri = object.getInt("kiri");
    maju = object.getInt("maju");
    mundur = object.getInt("mundur");
    berhenti = object.getInt("stop");
    Serial.print("*");
    Serial.print(maju);
    Serial.print(",");
    Serial.print(mundur);
    Serial.print(",");
    Serial.print(kanan);
    Serial.print(",");
    Serial.print(kiri);
    Serial.print(",");
    Serial.print(berhenti);
    Serial.println("#");
  }
}

void updatePpm(int data) {
  sprintf(buffer, "sensor/%d", num);
  updateData["ppm"] = data;
  updateData["timestamp"] = timestamp;

  Firebase.set(buffer, updateData);
  Firebase.set("id", num);

  //  Serial.println();
  //  Serial.println("Data update");

  updateData.prettyPrintTo(Serial);
  Serial.println();
  if (Firebase.failed()) {
    Serial.print("setting time failed:");
    Serial.println(Firebase.error());
    return;
  }
  num++;
}

void gas() {
  ppm = analogRead(asap);
  delay(100);
}

